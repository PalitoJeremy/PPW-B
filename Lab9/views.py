from django.shortcuts import render
from django.http import HttpResponse
import requests
import json

# Create your views here.
def index (request):
    return render(request, "Story9.html")

def data(request):
    getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    jsonParse = json.dumps(getJson.json())
    return HttpResponse(jsonParse)

