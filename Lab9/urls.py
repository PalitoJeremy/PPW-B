from django.conf.urls import url
from .views import index, data

#url for app

urlpatterns = [
    url(r'^$', index),
    url(r'^/data/', data, name='data'),
]