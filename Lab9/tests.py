from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest


# Create your tests here.
class Lab9UnitTest(TestCase):
    def test_lab9_using_as_template(self):
        response = Client().get('/lab9/')
        self.assertTemplateUsed(response, 'lab9.html')

    def test_lab9_url_exists(self):
        response = Client().get('/lab9/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/lab9/data/')
        self.assertEqual(response.status_code, 200)