from django.contrib import admin
from django.urls import path
from .views import *


urlpatterns = [
	path('', story8, name='story8'),
	path(r'^Story8/', story8, name='story8'),
]
